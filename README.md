# MNIST_sequence_generator

Browser based application to visualize numerical sequences as sets of
randomly selected images from the MNIST database. A node-based server
delivers the client code to the browser and then continues to handle
requests for digits from the client.
