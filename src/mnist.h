#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <Eigen/Dense>

struct int32 {
  unsigned short value;
  int32() : value(0) { }
  int32(std::istream & in) : value(0) {
    for (int i=0; i<4; ++i) {
      unsigned char c = in.get();
      value = value*256 + c;
    }
  }
  operator unsigned short() const { return value; }
};

struct label {
  unsigned char value;
  label() : value(0) { }
  label(std::istream & in) : value(0) {
    value = in.get();
  }
  std::istream & input(std::istream & lhs) {
    value = lhs.get();
    return lhs;
  }
  std::ostream & output(std::ostream & lhs) const {
    lhs << int(value);
    return lhs;
  }
};

inline std::istream & operator>>(std::istream & lhs, label & rhs) {
  return rhs.input(lhs);
}

inline std::ostream & operator<<(std::ostream & lhs, const label & rhs) {
  return rhs.output(lhs);
}

struct pixel {
  unsigned char value;
public:
  pixel() : value(0) { }
  pixel(char c) : value(c) { }
  pixel(std::istream & in) { value = in.get(); }
  std::istream & input(std::istream & lhs) {
    value = lhs.get();
    return lhs;
  }
  std::ostream & output(std::ostream & lhs) const {
    int tmp = value/26;
    lhs << (tmp < 1 ? " " : tmp < 3 ? "~" : tmp < 7 ? "+" : "#");
    return lhs;
  }
  operator unsigned char() const { return value; }
};

inline std::istream & operator>>(std::istream & lhs, pixel & rhs) {
  return rhs.input(lhs);
}

inline std::ostream & operator<<(std::ostream & lhs, const pixel & rhs) {
  return rhs.output(lhs);
}

typedef Eigen::Matrix<float,28,28> ImageVec;

struct image : public std::vector<pixel> {
  unsigned short numRows, numCols, imgSize;
public:
  image() : std::vector<pixel>(28*28, 0),
    numRows(28), numCols(28), imgSize(28*28) { }
  image(int32 nr, int32 nc, std::istream & in) : std::vector<pixel>(nr*nc, 0),
    numRows(nr), numCols(nc), imgSize(nr*nc) {
    for (int i=0; i<imgSize; ++i) {
      this->at(i).input(in);
    }
  }
  std::istream & input(std::istream & lhs) {
    return lhs;
  }
  std::ostream & output(std::ostream & lhs) const {
    lhs << numRows << "x" << numCols << std::endl;
    for (int r=0, i=0; r<numRows; ++r) {
      for (int c=0; c<numCols; ++c, ++i) {
        lhs << this->at(i);
      }
      lhs << std::endl;
    }
    return lhs;
  }
  ImageVec imageVec() const {
    ImageVec ans;
    for (int i=0; i<imgSize; ++i) {
      ans(i) = float(this->at(i)/255.0f);
    }
    return ans;
  }
};

inline std::istream & operator>>(std::istream & lhs, image & rhs) {
  return rhs.input(lhs);
}

inline std::ostream & operator<<(std::ostream & lhs, const image & rhs) {
  return rhs.output(lhs);
}

struct DataSet {
  std::vector<label> lbls;
  std::vector<image> imgs;
  
public:
  DataSet() { }
  
  void readLabels(std::string file) {
    std::ifstream fin(file.c_str(), std::ifstream::binary);
    int32 magic(fin);
    if (magic != 2049) std::cerr << "Not a valid label file." << std::endl;
    int32 numLbls(fin);
    lbls.resize(numLbls);
    for (int i=0; i<numLbls; ++i) {
      fin >> lbls[i];
    }
  }
  
  void readImages(std::string file) {
    std::ifstream fin(file.c_str(), std::ifstream::binary);
    int32 magic(fin);
    if (magic != 2051) std::cerr << "Not a valid image file." << std::endl;
    int32 numImgs(fin);
    int32 numRows(fin);
    int32 numCols(fin);
    int imgSize(numRows*numCols);
    imgs.reserve(numImgs);
    for (int i=0; i<numImgs; ++i) {
      imgs.push_back(image(numRows, numCols, fin));
    }
  }
};

