#include <iostream>
#include <Eigen/Dense>
#include "mnist.h"

using namespace std;
using namespace Eigen;

int main(int argc, char ** argv) {
  MatrixXd m(2,2);
  m(0,0) =  3.0;
  m(1,0) =  2.5;
  m(0,1) = -1.0;
  m(1,1) = m(1,0) + m(0,1);
  cout << m << endl;

  DataSet mnist;
  mnist.readLabels("../t10k-labels-idx1-ubyte");
  mnist.readImages("../t10k-images-idx3-ubyte");
  cout << mnist.imgs[7] << endl;
  cout << mnist.lbls[7] << endl;

  cout << mnist.imgs[7].imageVec() << endl;
  return 0;
}
