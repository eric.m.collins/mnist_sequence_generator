'use strict';

var numRows     = 28;
var numCols     = 28;
var imgSize     = numRows*numCols;
var NI          = 5;
var NJ          = 2;
var N           = NI*NJ;
var trainSet    = undefined;
var testSet     = undefined;
var container   = undefined;
var seqCanvas   = undefined;
var tgtCanvas   = undefined;
var focusCanvas = undefined;
var seqContext  = undefined;
var tgtContext  = undefined;
var seqData     = undefined;
var tgtData     = undefined;
var seqUpdate   = false;
var currSeq     = [];
var scaleFactor = 5;
var x0          = 0;
var y0          = 0;

window.addEventListener( 'load', init, false );
window.addEventListener( 'resize', onWindowResize, false );
window.addEventListener( 'keydown', onKeyDown, false );

// Recieve image data buffer and convert to valid ImageData
// object. Render ImageData to canvas.
function parseImageData(event)  {
  var view = new DataView(this.response);
  var img = new ImageData(numCols, numRows);
  for (var i=0, p=0, q=0; i<numRows; ++i) {
    for (var j=0; j<numCols; ++j, ++q) {
      for (var k=0; k<3; ++k) {
        img.data[p++] = view.getUint8(q);
      }
      img.data[p++] = 255;
    }
  }
  // Store this image in trainSet at a location corresponding to the
  // current sequence.
  var lbl = view.getUint8(q) - 48;
  for (var i=0; i<currSeq.length; ++i) {
    if ( trainSet[i] === undefined && currSeq[i] == lbl ) {
      console.log("Assigning image of", lbl, "to trainSet[", i, "]");
      trainSet[i] = img;
      break;
    }
  }
  // Request rendering update
  seqUpdate = true;
}

function imageSmoothing(context, val) {
  context.imageSmoothingEnabled = val;
  // context.mozImageSmoothingEnabled = val;
  context.webkitImageSmoothingEnabled = val;
  context.msImageSmoothingEnabled = val;
}

function init() {
  trainSet = new Array(N);
  
  container = document.getElementById('container');
  
  seqCanvas = document.createElement('canvas');
  seqCanvas.id = 'seqArea';
  seqCanvas.width = NI*numCols;
  seqCanvas.height = NJ*numRows;
  seqCanvas.addEventListener('mousemove', onSeqMouseMove);
  seqContext = seqCanvas.getContext('2d');
  imageSmoothing(seqContext, false);
  container.appendChild(seqCanvas);
  
  tgtCanvas = document.createElement('canvas');
  tgtCanvas.id = 'tgtArea';
  tgtCanvas.width = numRows*scaleFactor;  // Enlarge tgtArea by scaleFactor
  tgtCanvas.height = numCols*scaleFactor;
  tgtContext = tgtCanvas.getContext('2d');
  imageSmoothing(tgtContext, false);
  container.appendChild(tgtCanvas);

  for (var i=0; i<N; ++i) {
    var num = Math.floor(10*Math.random());
    currSeq.push(num);
    console.log("Requesting image of:", num);
    var oReq = new XMLHttpRequest();
    oReq.responseType = "arraybuffer";
    oReq.onload = parseImageData;
    oReq.open("GET", "train?"+num);
    oReq.send();
  }

  animate();
}

//--------------------------------------------------------------------
function onWindowResize() {
  // Request rendering update
  seqUpdate = true;
}
//--------------------------------------------------------------------
function onKeyDown(event) {
  switch (event.key) {
  }
  // Request rendering update
  seqUpdate = true;
}
//--------------------------------------------------------------------
function onSeqMouseMove(event) {
  x0 = Math.min((NI-1)*numCols,Math.max(0,event.layerX-numCols/2));
  y0 = Math.min((NJ-1)*numRows,Math.max(0,event.layerY-numRows/2));
  // Request rendering update
  seqUpdate = true;
};
//--------------------------------------------------------------------
function animate() {
  requestAnimationFrame(animate);
	if (seqUpdate) render();
}
//--------------------------------------------------------------------
function render() {
  seqContext.clearRect(0, 0, seqCanvas.width, seqCanvas.height);
  // Render the current sequence
  for (var i=0; i<currSeq.length; ++i) {
    if (trainSet[i] !== undefined) {
      var x = parseInt(numCols*(i%NI));
      var y = parseInt(numRows*Math.floor(i/NI));
      seqContext.putImageData(trainSet[i], x, y);
    }
  }
  
  // Render current attention area
  tgtContext.drawImage( seqCanvas,
                        Math.min(Math.max(0,x0)),
                        Math.min(Math.max(0,y0)),
                        numCols,numRows,
                        0, 0,
                        tgtCanvas.width, tgtCanvas.height );
  // Render the current attention reticule
  seqContext.strokeStyle = "green";
  seqContext.lineWidth = 2;
  seqContext.strokeRect(x0,y0,numCols,numRows);
  // Cancel the update request
  // seqUpdate = false;
}
